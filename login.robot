*** Settings ***
Documentation    Suite description
Library   Collections
Library  SeleniumLibrary


*** Test Cases ***
Login with Invalid Credentials
    [Tags]    TC1
    Open Browser  https://login.optimyapp.com  chrome
    Set Text Only  ${user}  qatest1@gmail.com
    Set Text Only  ${pass}  qatest
    Click  ${submit}
    Wait Until Element Is Visible  ${invalidlogin}  10

Login with Invalid Empty Credentials
    [Tags]    TC2
    Open Browser  https://login.optimyapp.com  chrome
    Click  ${submit}
    Wait Until Element Is Visible  ${requireduser}  10
    Wait Until Element Is Visible  ${requiredpass}  10

Login with Empty Email
    [Tags]    TC3
    Open Browser  https://login.optimyapp.com  chrome
    Set Text Only  ${pass}  qatest
    Click  ${submit}
    Wait Until Element Is Visible  ${requireduser}  10

Login with Empty Password
    [Tags]    TC4
    Open Browser  https://login.optimyapp.com  chrome
    Set Text Only  ${user}  qatest1@gmail.com
    Click  ${submit}
    Wait Until Element Is Visible  ${requiredpass}  10

Login with Incorrect format of email
    [Tags]    TC5
    Open Browser  https://login.optimyapp.com  chrome
    Set Text Only  ${user}  qatest
    Click  ${submit}
    Wait Until Element Is Visible  ${requireduser}  10

*** Keywords ***

Click
    [Arguments]    ${locator}
    Click Element  ${locator}

Wait then Click
    [Arguments]    ${locator}
    wait until element is visible  ${locator}  60
    sleep  1
    SeleniumLibrary.Click Element  ${locator}

Set Text Only
    [Arguments]  ${locator}  ${text}
    SeleniumLibrary.Input Text  ${locator}  ${text}

Wait Until Element Is Visible
    [Arguments]  ${locator}  ${timeout}
    SeleniumLibrary.Wait Until Element Is Visible  ${locator}  ${timeout}

*** Variables ***
${user}  //*[@id="tab-login"]/form/div[1]/input
${pass}  //*[@id="tab-login"]/form/div[2]/input
${submit}  //*[@id="tab-login"]/form/button
${invalidlogin}  //*[@id="login-invalid"]
${requireduser}  //*[@id="tab-login"]/form/div[1]/span
${requiredpass}  //*[@id="tab-login"]/form/div[2]/span
